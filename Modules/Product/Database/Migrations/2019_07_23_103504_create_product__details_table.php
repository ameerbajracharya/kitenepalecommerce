<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product__details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsigned('productId');
            $table->text('description');
            $table->string('features');
            $table->string('capacity');
            $table->string('services');
            $table->string('colors');
            $table->string('size');
            $table->string('caption');
            $table->string('keyword');
            $table->string('metaTag');
            $table->string('metaDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product__details');
    }
}
