<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('productName');
            $table->string('code');
            $table->string('qrCode');
            $table->string('barCode');
            $table->unsigned('categoryId');
            $table->unsigned('brandId');
            $table->unsigned('typeId');
            $table->unsigned('unitId');
            $table->unsigned('altUnitId');
            $table->integer('wholeSalePrice');
            $table->integer('markedPrice');
            $table->integer('sellingPrice');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
