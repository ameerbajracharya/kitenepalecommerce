<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product__sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsigned('productId');
            $table->string('discountPercent');
            $table->integer('discountAmount');
            $table->date('startDate');
            $table->date('endDate');
            $table->boolean('status');
            $table->string('caption');
            $table->string('keyword');
            $table->string('metaTag');
            $table->string('metaDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product__sales');
    }
}
