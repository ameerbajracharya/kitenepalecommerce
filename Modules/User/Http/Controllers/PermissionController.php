<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Session;

class PermissionController extends Controller
{
	public function index()
	{
		$data['permission'] = Permission::all();
		return view('user::permissions.index',compact('data'));
	}

	public function create()
	{
		$data['permission'] = Permission::all();
		return view('user::permissions.create', compact('data'));
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|unique:permissions,name',
		]);
		$data['permission'] = Permission::create(["name" => $request->name]);
		Session::flash('success', 'Permission Created Successfully.');
		return redirect()->route('permission.index',compact('data'));
	}


	public function edit($id)
	{
		$data['permission'] = Permission::find($id);
		return view('user::permissions.edit', compact('data'));
	}

	public  function update(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required|unique:permissions,name',
		]);
		$data['permission'] = Permission::update(["name" => $request->name]);
		Session::flash('success', 'Permission Updated Successfully.');
		return redirect()->route('permission.index',compact('data'));	
	}

	public function delete($id)
	{
		$data['permission'] = Permission::find($id);
		$data['permission']->delete();
		Session::flash('success', 'Permission Deleted Successfully.');
		return redirect()->route('permission.index',compact('data'));	
	}

}
